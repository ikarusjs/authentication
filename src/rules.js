const { authenticate } = require('@feathersjs/authentication').hooks
const { setField } = require('feathers-authentication-hooks')
const { normalizeHooks } = require('@ikarusjs/tools')
const { Rule } = require('../../ikarus/src')

class NeedsAuthenticationRule extends Rule {
  trigger() {
    return 'before'
  }
  get hook() {
    return authenticate(...this.args)
  }
}

class RestrictToOwnerRule extends Rule {
  constructor({ local_key = 'id', foreign_key = 'userId' } = {}) {
    super()
    this.local_key = local_key
    this.foreign_key = foreign_key
  }
  trigger() {
    return 'before.update|patch|remove|find|get'
  }
  get hook() {
    return setField({ from: 'params.user.' + this.local_key, as: 'params.query.' + this.foreign_key })
  }
  publish(data) {
    return `authenticated.${data[this.foreign_key]}`
  }
}

class AssociateCurrentUserRule extends Rule {
  constructor({ local_key = 'id', foreign_key = 'userId' } = {}) {
    super()
    this.local_key = local_key
    this.foreign_key = foreign_key
  }
  trigger() {
    return 'before.update|patch|create'
  }
  get hook() {
    return setField({ from: 'params.user.' + this.local_key, as: 'data.' + this.foreign_key })
  }
}

module.exports = {
  NeedsAuthenticationRule,
  RestrictToOwnerRule,
  AssociateCurrentUserRule
}
