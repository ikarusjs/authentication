const { ServiceProvider } = require('@ikarusjs/ikarus')

module.exports = class AuthenticationServiceProvider extends ServiceProvider {
  boot() {
    this.app.on('connection', connection => {
      this.app.channel('anonymous').join(connection)
      this.app.channel('connections').join(connection)
    })
    this.app.on('login', (authResult, { connection }) => {
      // connection can be undefined if there is no
      // real-time connection, e.g. when logging in via REST
      if(connection) {
        this.app.channel('anonymous').leave(connection)
        this.app.channel('authenticated').join(connection)
        this.app.channel(`authenticated.${connection.user.id}`).join(connection)
      }
    })
  }
}
