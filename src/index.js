module.exports = {
  Endpoint: require('./endpoint'),
  strategies: require('./stategies'),
  rules: require('./rules'),
  AuthenticationServiceProvider: require('./AuthenticationServiceProvider'),
}
