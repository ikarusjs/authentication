const { AuthenticationService } = require('@feathersjs/authentication')
const { Endpoint } = require('@ikarusjs/ikarus')
const inflector = require('inflector-js')

function assignStrategyOptions(options, strategy_options) {
  options[strategy_options.name] = {}

  if(strategy_options.allowTokenGeneration) {
    options.authStrategies.push(strategy_options.name)
  }

  for(const propertyname in strategy_options) {
    if(!['name', 'allowTokenGeneration'].includes(propertyname)) {
      options[strategy_options.name][propertyname] = strategy_options[propertyname]
    }
  }

  return options
}

module.exports = class extends Endpoint {

  generateService() {
    const strategies = this.constructor.strategies()
    let options = this.constructor.options()
    const config_key = this.constructor.configKey()

    for(const { options: strategy_options } of strategies) {
      options = assignStrategyOptions(options, strategy_options)
    }
    const authservice = new AuthenticationService(this.app, config_key, options)

    for(const { strategy, options } of strategies) {
      authservice.register(options.name, new strategy())
    }

    return authservice
  }

  static entity() {
    return this.name.slice(0, this.name.indexOf('Authentication')).toLowerCase()
  }

  static endpoint() {
    return inflector.pluralize(this.entity())
  }

  static configKey() {
    return 'authentication'
  }

  static strategies() {
    return []
  }

  static options() {
    return {
      service: this.endpoint(),
      entity: this.entity(),
      authStrategies: [],
    }
  }

}

// module.exports = function({ endpoint, entity, strategies = [], config_key, options = {} }) {
//   return app => ({
//     service: () => {
//       options = Object.assign({
//         service: endpoint,
//         entity: inflector.singularize(endpoint),
//         authStrategies: []
//       }, options)

//       for(const { options: strategy_options } of strategies) {
//         options = assignStrategyOptions(options, strategy_options)
//       }

//       const authservice = new AuthenticationService(app, config_key, options)

//       for(const { strategy, options } of strategies) {
//         authservice.register(options.name, new strategy())
//       }

//       return authservice
//     }
//   })
// }
