const { JWTStrategy } = require('@feathersjs/authentication')
const { LocalStrategy } = require('@feathersjs/authentication-local')

base_options = {
  allowTokenGeneration: true,
}

module.exports = {
  JWTStrategy(options = {}) {
    options = Object.assign({}, base_options, {
      name: 'jwt',
    }, options)

    return { strategy: JWTStrategy, options }
  },
  LocalStrategy(options = {}) {
    options = Object.assign({}, base_options, {
      name: 'local',
    }, options)

    return { strategy: LocalStrategy, options }
  },
}
