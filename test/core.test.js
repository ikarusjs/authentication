const expect = require('expect.js')
const { Ikarus, ServiceProviders } = require('@ikarusjs/ikarus')
const { strategies, Endpoint, AuthenticationServiceProvider, rules: { AssociateCurrentUserRule } } = require('../src/')
const { Endpoint: ClassEndpoint } = require('@ikarusjs/ikarus')

describe('authentication package', () => {
  const auth_endpoint = class UserAuthentication extends Endpoint {
    static strategies() {
      return [
        strategies.JWTStrategy({ allowTokenGeneration: false }),
        strategies.LocalStrategy({ usernameField: 'name', passwordField: 'password' }),
      ]
    }
    static configKey() {
      return 'authentication-config-key'
    }
  }

  const user_endpoint = class Users extends ClassEndpoint {
    generateService() {
      return {
        async create(data) {
          return data
        }
      }
    }
    static rules() {
      return [
        new AssociateCurrentUserRule()
      ]
    }
  }

  it('registers auth endpoint', async () => {
    class App extends Ikarus {
      endpoints() { return { '/authentication': auth_endpoint, '/users': user_endpoint } }
      providers() { return [ ServiceProviders.SocketioServiceProvider, AuthenticationServiceProvider ] }
    }
    const app = await App.create()

    expect(app.service('/authentication')).to.be.an('object')
    expect(app.get('authentication-config-key')).to.have.property('service')
    expect(app.get('authentication-config-key').service).to.be('users')

    expect(app.get('authentication-config-key')).to.have.property('local')
    expect(app.get('authentication-config-key').local).to.have.property('usernameField')
    expect(app.get('authentication-config-key').local.usernameField).to.be('name')

    expect(app.get('authentication-config-key')).to.have.property('authStrategies')
    expect(app.get('authentication-config-key').authStrategies).to.be.an('array')
    expect(app.get('authentication-config-key').authStrategies).to.contain('local')
    expect(app.get('authentication-config-key').authStrategies).not.to.contain('jwt')

    expect(auth_endpoint.entity()).to.be('user')
    expect(auth_endpoint.endpoint()).to.be('users')
  })
})
